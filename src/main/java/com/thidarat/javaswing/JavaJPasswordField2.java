/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.javaswing;

import static java.awt.SystemColor.text;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author User
 */
public class JavaJPasswordField2 {

    public static void main(String[] args) {
        JFrame f = new JFrame("Password Field Example");

        final JLabel label = new JLabel();
        label.setBounds(20, 150, 200, 50);
        f.add(label);

        final JPasswordField value = new JPasswordField();
        value.setBounds(100, 75, 100, 30);
        f.add(value);

        JLabel l1 = new JLabel("Username:");
        l1.setBounds(20, 20, 80, 30);

        JLabel l2 = new JLabel("Password:");
        l2.setBounds(20, 75, 80, 30);

        JButton b = new JButton("Login");
        b.setBounds(100, 120, 80, 30);

        f.add(l1);
        f.add(l2);
        f.add(b);

        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        
        b.addActionListener(new ActionListener() {  
            
                @Override
                public void actionPerformed(ActionEvent e) {       
                   String data = "Username " + text;  
                   data += ", Password: "   
                   + new String(value.getPassword());   
                   label.setText(data);          
                }  

           
             });   
    }
}
