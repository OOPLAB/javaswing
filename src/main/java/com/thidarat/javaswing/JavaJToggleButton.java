/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.javaswing;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JFrame;
import javax.swing.JToggleButton;

/**
 *
 * @author User
 */
public class JavaJToggleButton extends JFrame  implements ItemListener {

    private JToggleButton button;

    JavaJToggleButton() {

        this.setTitle("JToggleButton with ItemListener Example");  
        this.setLayout(new FlowLayout());  
        this.setJToggleButton();  
        this.setAction();  
        
        
        this.setSize(200, 200);  
        this.setVisible(true);  
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
    }
    
     private void setJToggleButton() {  
        button = new JToggleButton("ON");  
        add(button);  
    }  
     
     private void setAction() {  
        button.addItemListener(this);  
    }  
     
    @Override
      public void itemStateChanged(ItemEvent eve) {  

        if (button.isSelected())  
            button.setText("OFF");  
        else  
            button.setText("ON");  
      
}  
    public static void main(String[] args) {
        new JavaJToggleButton();
    }
    
}
