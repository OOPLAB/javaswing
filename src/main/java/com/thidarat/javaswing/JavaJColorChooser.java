/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.javaswing;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class JavaJColorChooser extends JFrame implements ActionListener {

    JButton b;
    Container c;

    JavaJColorChooser() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);  
        c = getContentPane();
        c.setLayout(new FlowLayout());

        b = new JButton("color");
        b.addActionListener(this);
        c.add(b);

        this.setSize(400, 400);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        Color initialcolor = Color.RED;
        Color color = JColorChooser.showDialog(this, "Select a color", initialcolor);
        c.setBackground(color);
    }

    public static void main(String[] args) {
        new JavaJColorChooser();

    }
}
