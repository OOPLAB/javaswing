/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author User
 */
public class JavaJComboBox {

    JFrame f;

    JavaJComboBox() {

        f = new JFrame("ComboBox Example");

        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        f.add(label);

        JButton b = new JButton("Show");
        b.setBounds(200, 100, 75, 20);
        String languages[] = {"C", "C++", "C#", "Java", "PHP"};
        f.add(b);

        final JComboBox cb = new JComboBox(languages);
        cb.setBounds(50, 100, 90, 20);
        f.add(cb);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Programming language Selected: "
                        + cb.getItemAt(cb.getSelectedIndex());
                label.setText(data);
            }
        });

        f.setLayout(null);
        f.setSize(350, 350);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new JavaJComboBox();
    }
}
