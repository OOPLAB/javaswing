/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.javaswing;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author User
 */
public class JavaJCheckBox2 {

    JavaJCheckBox2() {
        JFrame f = new JFrame("CheckBox Example");

        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        f.add(label);

        JCheckBox checkbox1 = new JCheckBox("C++");
        checkbox1.setBounds(150, 100, 50, 50);
        f.add(checkbox1);

        JCheckBox checkbox2 = new JCheckBox("Java");
        checkbox2.setBounds(150, 150, 50, 50);
        f.add(checkbox2);

        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        
        checkbox1.addItemListener(new ItemListener() {    
             @Override
             public void itemStateChanged(ItemEvent e) {                 
                label.setText("C++ Checkbox: "     
                + (e.getStateChange()==1?"checked":"unchecked"));    
             }    
          });    
        
        checkbox2.addItemListener(new ItemListener() {    
             @Override
             public void itemStateChanged(ItemEvent e) {                 
                label.setText("Java Checkbox: "     
                + (e.getStateChange()==1?"checked":"unchecked"));    
             }    
          });    
        
    }

    public static void main(String[] args) {
        new JavaJCheckBox2();
    }
}
